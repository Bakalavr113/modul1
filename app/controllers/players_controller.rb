class PlayersController < ApplicationController
  def index
  	@players=Player.all
  end
  def show 
  	@player=Player.find(params[:id])
  end
  def new
  	@player=Player.new
  end
  def create
  	@player=Player.new(set_params)
  	if @player.save
  		redirect_to @player
  	else render new
  	end
  end
  
  def edit 
  	@player=Player.find(params[:id])
  end
  def update
  	@player=Player.find(params[:id])
  	if player.update_attributes(set_params)
  		redirect_to @player
  	else render edit
  	end

end
def destroy 
	@player=Player.find(params[:id])
	@player.destroy
	redirect_to players_path
	end
	private 
  def set_params
  	params.require(:player).permit(:nickname, :rank, :charisma, :wisdom)
  end	
end
